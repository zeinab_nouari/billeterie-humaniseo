package controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class panneauPuzzle extends JPanel{


    public static final int NB_PIECE_PAR_DEFAUT = 9;

    private JButton[] bouton = new JButton[NB_PIECE_PAR_DEFAUT];
    private Icon[] image = new Icon[NB_PIECE_PAR_DEFAUT];
    public int compteur = 0;

    //panneauCommande panneauCommande;

    Insets emptyInsets = new Insets(0, 0, 0, 0);

    //Constructeur
    public panneauPuzzle(){
        super();
        this.setPreferredSize(new Dimension(815,568));
        this.setLayout(new GridLayout(3, 3));
        this.setVisible(true);
        initComponents();
    }

    //Méthodes
    public void initComponents(){
        compteur = 0;
    }

    public void remplirFenetre(String dossier){
        for(int i=0; i<9;i++) {
            image[i] = new ImageIcon(dossier +"0" + i + ".jpg");
            bouton[i] = new JButton(image[i]);

            if (i == 8) {
                bouton[i].setMargin(emptyInsets);
                bouton[i].setBorder(null);
            }

            bouton[i].addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    //System.out.println("pressed");
                    JButton boutonSelect = (JButton) e.getSource();

                    switchButton(boutonSelect);
                    //melangerBoutons();
                    //compteur();

                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    //System.out.println("released");
                    //compteur();
                }
            });
            this.add(bouton[i]);
        }
    }


    public void switchButton(JButton boutonSelect) {
        int x_select = boutonSelect.getX();
        int y_select = boutonSelect.getY();
        //Bouton[8] : case blanche

        //saut de 2 en vertical
        if((((boutonSelect.getX() == bouton[8].getX()) || (boutonSelect.getY() == bouton[8].getY())) && ((boutonSelect.getY()+bouton[8].getY() != 352)) || ((boutonSelect.getY()) == 176 && (bouton[8].getY() == 176)))){
            if((boutonSelect.getX() == 0 && bouton[8].getX() == 532) || (boutonSelect.getX() == 532 && bouton[8].getX() == 0)){
                //saut de 2 en horizontale
            }else{

                boutonSelect.setLocation(bouton[8].getX(), bouton[8].getY());
                bouton[8].setLocation(x_select, y_select);
                this.repaint();

                this.compteur += 1;

                this.estCorrect();
            }
        }

    }


    public int getCompteur(){
        return this.compteur;
    }

    public void setCompteur(int compteur){
        this.compteur = compteur;
    }

    public boolean estCorrect(){
        if(((bouton[0].getX() == 0) && (bouton[0].getY() == 0)) &&
                ((bouton[1].getX() == 266) && (bouton[1].getY() == 0)) &&
                ((bouton[2].getX() == 532) && (bouton[2].getY() == 0)) &&
                ((bouton[3].getX() == 0) && (bouton[3].getY() == 176)) &&
                ((bouton[4].getX() == 266) && (bouton[4].getY() == 176)) &&
                ((bouton[5].getX() == 532) && (bouton[5].getY() == 176)) &&
                ((bouton[6].getX() == 0) && (bouton[6].getY() == 352)) &&
                ((bouton[7].getX() == 266) && (bouton[7].getY() == 352)) &&
                ((bouton[8].getX() == 532) && (bouton[8].getY() == 352))){


            JOptionPane.showMessageDialog(this,"Bravo ! Tu as réussi le puzzle en " + getCompteur() + " coups !");
            return true;
        }else{
            return false;
        }
    }


    public void melangerBoutons() {
        setCompteur(0);
        for (int i = 0 ; i < 20 ; i++){
            // On prend deux cases au hasard
            int rdmCase = (int)(Math.random() * 8);
            int rdmCase2 = (int)(Math.random() * 8);

            if (rdmCase != rdmCase2){
                int x_save = bouton[rdmCase].getX();
                int y_save = bouton[rdmCase].getY();
                int x_save2 = bouton[rdmCase2].getX();
                int y_save2 = bouton[rdmCase2].getY();

                bouton[rdmCase].setLocation(bouton[8].getX(), bouton[8].getY());
                bouton[8].setLocation(x_save, y_save);
                int x_bouton8 = bouton[8].getX();
                int y_bouton8 = bouton[8].getY();

                bouton[rdmCase2].setLocation(bouton[rdmCase].getX(), bouton[rdmCase].getY());
                bouton[rdmCase].setLocation(x_save2, y_save2);
                x_save2 = bouton[rdmCase2].getX();
                y_save2 = bouton[rdmCase2].getY();

                bouton[rdmCase2].setLocation(x_bouton8, y_bouton8);
                bouton[8].setLocation(x_save2, y_save2);
            }
            this.repaint();

        }
        System.out.println("mélangé");


    }

}


