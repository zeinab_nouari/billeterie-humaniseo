package model;



import vue.Puzzle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PageAccueil extends JFrame implements ActionListener {

        private JPanel pan = new JPanel();

        private JButton bouton1 = new JButton(new ImageIcon("montagnes.jpeg"));
        private JButton bouton2 = new JButton(new ImageIcon("enfants.jpg"));
        private JButton bouton3 = new JButton(new ImageIcon("ville.jpg"));
        private JLabel txt = new JLabel("Choisissez votre puzzle");
        private JLabel img_montagnes = new JLabel("Hymalaya");
        private JLabel img_enfants = new JLabel("Jeunes moines bouddhiste");
        private JLabel img_ville = new JLabel("Katmandou");


        public PageAccueil() {
                pan.setLayout(null);
                this.setTitle("Page d'accueil");
                this.setSize(815, 568);
                this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                this.setLocationRelativeTo(null);

                txt.setBounds(260,50,400,50);
                txt.setFont(new Font("Arial Black", Font.BOLD,20));
                pan.add(txt);


                bouton1.setBounds(50, 142, 200, 200);
                pan.add(bouton1);
                img_montagnes.setBounds(113, 350, 200, 30);
                img_montagnes.setFont(new Font("Impact",Font.BOLD,15));
                pan.add(img_montagnes);

                bouton2.setBounds(300, 142, 200, 200);
                pan.add(bouton2);
                img_enfants.setBounds(303, 350, 200, 30);
                img_enfants.setFont(new Font("Impact", Font.BOLD,15));
                pan.add(img_enfants);

                bouton3.setBounds(550, 142, 200, 200);
                pan.add(bouton3);
                img_ville.setBounds(608, 350, 200, 30);
                img_ville.setFont(new Font("Impact", Font.BOLD,15));
                pan.add(img_ville);


                setContentPane(pan);

                bouton1.addActionListener(this);
                bouton2.addActionListener(this);
                bouton3.addActionListener(this);

                this.setVisible(true);
        }

        public void actionPerformed(ActionEvent evt) {
                //Lorsque nous cliquons sur notre bouton, on passe a l'autre fenétre
                this.dispose();
                Object source = evt.getSource();
                if (source == bouton1) {
                        Puzzle puzzle = new Puzzle("Les montagnes du Népal", "Puzzle/src/montagnes","Puzzle/src/montagnes_miniature.JPG");
                }
                else if (source == bouton2) {
                        Puzzle puzzle = new Puzzle("Jeunes moines bouddhiste", "Puzzle/src/enfants","Puzzle/src/enfants_miniature.PNG");
                }
                else if (source == bouton3) {
                        Puzzle puzzle = new Puzzle("Katmandou, la capitale du Népal", "Puzzle/src/ville","Puzzle/src/ville_miniature.PNG");
                }
        }

}