package vue;

import controller.panneauPuzzle;

import javax.swing.*;
import java.awt.*;

public class Puzzle extends JFrame{

    JFrame maFenetre = new JFrame("Puzzle");

    public Puzzle(String titre_puzzle, String dossier, String miniature) {

        panneauPuzzle puzzle = new panneauPuzzle();
        panneauCommande commande = new panneauCommande(puzzle,miniature);
        panneauTitre titre = new panneauTitre(this, titre_puzzle);

        //Dimension
        maFenetre.setPreferredSize(new Dimension(1185, 618));

        puzzle.remplirFenetre(dossier);
        puzzle.melangerBoutons();

        //Ajout des panneaux dans la fenêtre
        maFenetre.add(commande, BorderLayout.EAST);
        maFenetre.add(titre, BorderLayout.NORTH);
        maFenetre.add(puzzle, BorderLayout.CENTER);


        maFenetre.pack();
        maFenetre.setLocationRelativeTo(null);
        maFenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        maFenetre.setVisible(true);

     }
}


