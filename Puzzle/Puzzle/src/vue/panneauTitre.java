package vue;

import model.PageAccueil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class panneauTitre extends JPanel {

    JLabel titre1;
    JButton retour = new JButton("< Retour");
    vue.Puzzle frame_puzzle;

    public panneauTitre(Puzzle frame_puzzle, String titre){
        super();
        titre1 = new JLabel(titre);
        this.frame_puzzle = frame_puzzle;
        this.setBackground(Color.DARK_GRAY);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(815,50));

        initComponents();
    }

    public void initComponents (){

        titre1.setForeground(Color.white);
        titre1.setFont(new Font("Serif", Font.BOLD, 20));
        titre1.setBounds(450,8,300, 30);

        retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame_puzzle.maFenetre.dispose();
                Object source = e.getSource();
                if (source == retour) {
                    PageAccueil accueil = new PageAccueil();
                }
            }
        });

        retour.setBackground(Color.white);
        retour.setBounds(10,15,100, 20);

        this.add(retour);
        this.add(titre1);
    }

}

