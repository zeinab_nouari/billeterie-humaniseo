package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class panneauCommande extends JPanel{

    JLabel apercu = new JLabel("Aperçu");
    JButton melanger = new JButton("Mélanger");
    JButton boutonImage = new JButton();

    Icon image;
    int w;
    controller.panneauPuzzle panneauPuzzle;

    //Constructeur
    protected panneauCommande(controller.panneauPuzzle panneauPuzzle, String miniature){
        super();
        this.panneauPuzzle = panneauPuzzle;
        image = new ImageIcon(miniature);
        this.setBackground(Color.GRAY);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(370,568));

        initComponents();
    }

    //Méthodes
    public void initComponents (){
        melanger.setBounds(55,150,250, 40);
        melanger.setBackground(Color.DARK_GRAY);
        melanger.setForeground(Color.white);

        apercu.setFont(new Font("Serif", Font.BOLD, 15));
        apercu.setBounds(150,220,100, 30);

        boutonImage = new JButton(image);
        boutonImage.setBounds(60,250,image.getIconWidth(), image.getIconHeight());

        melanger.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                panneauPuzzle.melangerBoutons();
            }
        });

        this.add(apercu);
        this.add(melanger);
        this.add(boutonImage);
    }



}

