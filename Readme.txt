Explication de l'ensemble
	
Version 32 bits pour une meilleure compatibilité avec les machines étudiantes (pas besoin des modification dans le BIOS VB <= 6.0) 
	Serveur LAMP : Linux Debian 10 (buster) - Apache (2.4) - MariaDB (10.3) - PHP (7.3)
	Utilisateur vagrant / vagrant peut devenir root via un sudo -i sans mot de passe.
	Utilisateur gpi2 / network peut se connecter en SSH et déposer directement dans /var/www/html/gpi2 pour voir ses pages web à l'adresse http://192.168.56.80/gpi2/
	PhpMyAdmin (5.0.4) sans bug avec connexion possible en tant que gpi2 / network à l'adresse http://192.168.56.80/myadmin/
	les scripts système sont dans le répertoire scripts. Les fichiers sql sont dans le répertoire files (il ne faut pas mélanger les torchons et les serviettes  )

A priori un simple vagrant up et c'est bon.
