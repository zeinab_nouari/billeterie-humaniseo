package Controller;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import View.ViewQuizz ;

public class ActionChoiceHint {
    public String hint ;
    public JButton hintButton ;
    public ViewQuizz viewQuizz ;
    public boolean visible ;


    public ActionChoiceHint(ViewQuizz viewQuizz){
        this.viewQuizz = viewQuizz ;

    }

    public void showHint(String hint,JButton hintButton, JLabel hintLabel){
        hintButton.addActionListener((
                ActionEvent e) -> {
            hintButton.setVisible(false);
            hintLabel.setText(hint); });


    }
    public ViewQuizz getViewQuizz () {
        return this.viewQuizz;
    }
}