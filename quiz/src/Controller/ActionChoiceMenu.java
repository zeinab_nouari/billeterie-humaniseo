package Controller;

import View.ViewAbout;
import View.ViewHow;
import View.ViewMenu;
import View.ViewPlayer;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ActionChoiceMenu extends JFrame {
    static boolean go   ;
    ViewMenu welcome ;



    public ActionChoiceMenu(ViewMenu welcome) {

        this.welcome = welcome;
    }

    public void choose(int time){

        welcome.getPlay().addActionListener((ActionEvent e) -> {
            go = true;
            setVisible(false);
        });

        welcome.getHow().addActionListener((ActionEvent e) -> {
            new ViewHow(time);
        });

        welcome.getAbout().addActionListener((ActionEvent e) -> {
            new ViewAbout();
        });

        welcome.getExit().addActionListener((ActionEvent e) -> {
            System.exit(0);
        });

        while (!go) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException ex) {
            }
        }
        go  = false;


    }

}
