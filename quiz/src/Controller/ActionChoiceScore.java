package Controller;

import View.ViewScore;

import java.awt.event.ActionEvent;


public class ActionChoiceScore {
    private static boolean again =false;
    private ViewScore view;
    public ActionChoiceScore (ViewScore view){
        this.view = view;
    }

    public void choose () {

        ViewScore.getPlayAgain().addActionListener((ActionEvent e) -> {
            again= true ;
        });

        ViewScore.getExit().addActionListener((ActionEvent e) -> {
            System.exit(0);
        });

        while (!again) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException ex) {}
        }

        again = false ;

    }
}
