package Controller;

import View.ViewQuizz;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ActionChoiceQuizz {

    static boolean next = false ;
    public int score = 0 ;
    public ViewQuizz view ;
    public String answer ;

    public ActionChoiceQuizz(ViewQuizz view) {
        this.view = view;
    }

    public void setNext(boolean next){
        this.next = next ;
    }
    public void getAnswer(JButton option1, JButton option2,JButton option3,JButton option4, String answer, boolean next) throws InterruptedException {
        //this.score = score;
        this.answer = answer;
        this.next = next;
        option1.addActionListener((ActionEvent e) -> {
            if (option1.getText().equals(this.answer)) this.view.setScore(view.getScore()+1); ;
            this.setNext(true);
        });
        option2.addActionListener((ActionEvent e) -> {
            if (option2.getText().equals(this.answer)) this.view.setScore(view.getScore()+1);
            this.setNext(true);
        });
        option3.addActionListener((ActionEvent e) -> {
            if (option3.getText().equals(this.answer)) this.view.setScore(view.getScore()+1) ;
            this.setNext(true);
        });
        option4.addActionListener((ActionEvent e) -> {
            if (option4.getText().equals(this.answer)) this.view.setScore(view.getScore()+1);
            this.setNext(true);
        });
    }


    public boolean getNext(){
        return this.next ;
    }


}
