package Controller;

import View.ViewMenu;
import View.ViewPlayer;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ActionAddPlayer extends JPanel   {
    static boolean go  = false ;
    private JLabel res;
    ViewPlayer view;
    JFrame window = null;
    String prenom, nom;
    public ActionAddPlayer(ViewPlayer view){
        this.view = view;
        click();

    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public void click() {
        view.getSub().addActionListener((ActionEvent e) -> {
            prenom = view.getTName().getText();
            nom = view.getTlastN().getText();
            go = true;
            setVisible(false);
            view.dispose();

        });
        view.getReset().addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        while (!go) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException ex) {
            }
        }

        go = false;

    }
    /*public void actionPerformed (ActionEvent e){
            view.getSub().addActionListener(this);
            view.getReset().addActionListener(this);

            if (e.getSource() == view.getSub()) {
                go = true;
                setVisible(false);
                if (view.getTerm().isSelected()) {


                    res.setText("Inscription faite avec succès");
                } else {

                    view.getResadd().setText("");
                    res.setText("Acceptez les termes et conditions pour poursuivre");
                }
            } else if (e.getSource() == view.getReset()) {
                System.exit(0);
            }
            while (!go) {
                try {
                    Thread.sleep(0);
                } catch (InterruptedException ex) {
                }
            }


        }*/
    }
