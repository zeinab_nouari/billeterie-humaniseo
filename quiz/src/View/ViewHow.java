package View;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ViewHow extends JFrame {
    public ViewHow (int time) {
        setSize(400,400);
        setLocationRelativeTo(null);
        setResizable(false);
        JPanel pan = new JPanel () ;
        pan.setSize(this.getSize().width,this.getSize().height);
        pan.setBackground(new Color(163, 71, 116));
        pan.setLayout(null);
        setContentPane(pan);
        JLabel titre = new JLabel("<html><i><u><center> Comment ca marche </center></u></i>");
        JLabel comment = new JLabel("<html><br/><br>Vous avez "+ time/60+" min "+time%60 +" s pour répondre à 10 questions sur la ville d'Angers !<br/><br/>"
                + "Cliquez sur le point d'interrogation pour avoir un indice pour vous aider à répondre</html>");

        titre.setBounds(8,5,350,70);
        titre.setForeground(new Color(255, 255, 255));
        titre.setFont(new Font("Times", Font.BOLD, 18));
        comment.setFont(new Font("Times", Font.PLAIN, 15));
        comment.setBounds(8,20,350,160);
        comment.setForeground(Color.white);
        JLabel border = new JLabel() ;
        border.setBorder(new LineBorder(Color.lightGray, 2, true));
        border.setBounds(10,5,365,350);
        JLabel img = new JLabel(new ImageIcon(getClass().getResource("quizzHow.PNG")));
        img.setBounds(8,180,350,155);
        border.add(titre);
        border.add(comment);
        border.add(img);
        pan.add(border);
        setVisible(true);
    }
}
