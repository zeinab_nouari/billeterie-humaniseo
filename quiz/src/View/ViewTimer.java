package View;

import javax.swing.*;
import java.awt.*;
import model.Timer;

public class ViewTimer {

    public static JLabel timerLabel = new JLabel ("00 : 00 : 000") ;
    static Timer timer = new Timer();
    JLabel jpanel = new JLabel();
    static boolean next ;


    public ViewTimer(JLabel panel, Timer timer, JLabel temps){
        this.timer = timer ;
        this.timerLabel = temps ;
        this.jpanel = panel ;
        this.timerLabel.setBounds(150,10,300,50);
        this.timerLabel.setFont(new Font("Verdana", Font.BOLD, 40));
        this.timerLabel.setHorizontalAlignment(JLabel.CENTER);
        // this.timerLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.timerLabel.setForeground(new Color(100,75,60));

    }

    public void addTimer(){
        this.jpanel.add(timerLabel);
    }

    public void updateTimer(int max,JLabel timerLabel) throws InterruptedException {

        timerLabel.setText(String.format("%02d", timer.m)+" : "+String.format("%02d", timer.s)+" : "+String.format("%03d", timer.ms));
        timer.ms++ ;
        Thread.sleep(1);
        if (timer.ms==999){
            timer.s++ ;
            timer.ms=0 ;
        }
        if (timer.s==59){
            timer.m ++ ;
            timer.s=0;
        }

        if ((timer.s + timer.m*60) > max-3 ) {
            timerLabel.setForeground(Color.red);

            if ((timer.s + timer.m*60)==max) {
                return ;
            }
        }

    }

    public Timer getTime(){
        return this.timer ;
    }



}

