package View;

import Controller.ActionAddPlayer;
import model.Player;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewPlayer extends JFrame {
    Player player;
    private Container c;
    private JLabel title;
    private JLabel name;
    private static JTextField tname;
    private static JLabel lastN;
    private static JTextField tlastN;
    private static JButton sub;
    private static JButton reset;
    private static JTextArea resadd;
    private static JCheckBox term;
    private ImageIcon course;
    private static JLabel label;
    private JLabel res;
    static boolean go  = false ;



    public static JTextField getTName(){ return tname; }
    public static JButton getSub() { return sub; }
    public static JButton getReset() { return reset; }
    public JTextArea getResadd() { return resadd; }
    public static JTextField getTlastN() { return tlastN; }


    public ViewPlayer() {

        course = new ImageIcon(this.getClass().getResource("form.png"));
        label = new JLabel(course);
        label.setSize(50,50);
        label.setBounds(0,0,570,750);
        setTitle("Forme d'inscription");
        this.setSize(600,700);
        this.setLocationRelativeTo(null);
        this.pack();

        setBounds(430,80,600,700);
        setBackground(new Color(39, 138, 191));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        c = getContentPane();
        c.setLayout(null);
        c.add(label);

        title = new JLabel("FORME D'INSCRIPTION");
        title.setForeground(new Color(81, 58, 92));
        title.setFont(new Font("Arial", Font.BOLD, 30));
        title.setSize(400, 30);
        title.setLocation(120, 30);

        c.add(title);

        name = new JLabel("Prénom");
        name.setForeground(new Color(132, 72, 161));
        name.setFont(new Font("Arial", Font.PLAIN, 20));
        name.setSize(100, 20);
        name.setLocation(100, 150);

        c.add(name);

        tname = new JTextField();
        tname.setFont(new Font("Arial", Font.PLAIN, 15));
        tname.setSize(190, 20);
        tname.setLocation(200, 150);
        c.add(tname);

        lastN = new JLabel("NOM");
        lastN.setForeground(new Color(132, 72, 161));
        lastN.setFont(new Font("Arial", Font.PLAIN, 20));
        lastN.setSize(100, 20);
        lastN.setLocation(100, 200);

        c.add(lastN);

        tlastN = new JTextField();
        tlastN.setFont(new Font("Arial", Font.PLAIN, 15));
        tlastN.setSize(190, 20);
        tlastN.setLocation(200, 200);
        c.add(tlastN);

        sub = new JButton("Soumettre");
        sub.setFont(new Font("Arial", Font.PLAIN, 15));
        sub.setSize(200, 20);
        sub.setLocation(80, 550);
        c.add(sub);



        reset = new JButton("Annuler");
        reset.setFont(new Font("Arial", Font.PLAIN, 15));
        reset.setSize(200, 20);
        reset.setLocation(300, 550);
        c.add(reset);

        /*res = new JLabel("");
        res.setFont(new Font("Arial", Font.PLAIN, 20));
        res.setSize(200, 20);
        res.setLocation(270, 450);
        c.add(res);*/




        setVisible(true);
    }



    public JCheckBox getTerm() {
        return term;
    }

    public JLabel getRes() {
        return res;
    }
}
