package View;

import model.Score;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ViewScore extends JPanel {

    private static JButton playAgain ,
            exit ;

    private JLabel scoreLabel ;

    public static JButton getPlayAgain(){
        return playAgain;
    }
    public static JButton getExit(){
        return exit;
    }

    public ViewScore (JFrame window, int score) {

        setSize(window.getSize().width,window.getSize().height);
        setLayout(null);
        setBackground(new Color(86, 187, 156, 255));
        window.setContentPane(this);

        playAgain = new JButton ("Retour au Menu");
        playAgain.setForeground(new Color(86, 187, 156, 255)) ;
        playAgain.setBounds(200,250,200,50);
        playAgain.setBackground(Color.white);
        playAgain.setFont(new Font("Serif",Font.ITALIC,18));
        add(playAgain);

        exit = new JButton ("QUITTER");
        exit.setForeground(new Color(86, 187, 156, 255)) ;
        exit.setBounds(200,350,200,50);
        exit.setBackground(Color.white);
        exit.setFont(new Font("Serif",Font.ITALIC,18));
        add(exit);

        scoreLabel = new JLabel ("Votre score est de : "+ score +"/10");
        scoreLabel.setHorizontalAlignment(JLabel.CENTER);
        scoreLabel.setFont(new Font("Verdana", Font.BOLD, 40));
        scoreLabel.setForeground(Color.white);
        scoreLabel.setBorder(new LineBorder(Color.white, 2, true));
        scoreLabel.setBounds(0,100,600,100);
        add(scoreLabel);

        window.setVisible(true);

    }
}
