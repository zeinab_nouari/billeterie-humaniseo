package View;

import Controller.ActionAddPlayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewMenu extends JPanel{



    private ImageIcon course;
    private static JLabel label;
    static JButton
            play ,
            howItWorks ,
            about ,
            exit ;





    public static JButton getPlay(){
        return play;
    }
    public static JButton getHow(){
        return howItWorks;
    }
    public static JButton getAbout(){
        return about;
    }
    public static JButton getExit(){
        return exit;
    }



    public ViewMenu(JFrame window) {
        window.setSize(600,700);
        course = new ImageIcon(this.getClass().getResource("quizz.jpg"));
        label = new JLabel(course);
        label.setSize(500,500);
        label.setBounds(0,0,600,700);
        //window.setSize(window.getSize().width,window.getSize().height);
        label.setLayout(null);
        window.setBackground(new Color(39, 138, 191));
        window.setContentPane(this);
        window.add(label);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
        window.pack();


        play = new JButton("Commencez le quizz");
        play.setBackground(new Color(255, 255, 255));
        play.setBounds(180, 200, 200, 50);
        label.add(play);

        howItWorks = new JButton ("Comment ca marche");
        howItWorks.setBackground(new Color(255,255,255)) ;
        howItWorks.setBounds(180,300,200,50);
        label.add(howItWorks);

        about = new JButton ("Informations");
        about.setBackground(new Color(255,255,255)) ;
        about.setBounds(180,400,200,50);
        label.add(about);

        exit = new JButton ("QUITTER");
        exit.setBackground(new Color(255,255,255)) ;
        exit.setBounds(180,500,200,50);
        label.add(exit);




    }
}
