package View;
import javax.swing.JFrame;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ViewAbout extends JFrame{


        public ViewAbout() {
            setSize(400,400);
            setResizable(false);
            setLocationRelativeTo(null);
            JPanel pan = new JPanel () ;
            pan.setSize(this.getSize().width,this.getSize().height);
            pan.setBackground(new Color(224, 168, 80));
            pan.setLayout(null);
            setContentPane(pan);
            JLabel border = new JLabel() ;
            border.setBorder(new LineBorder(Color.white, 2, true));
            border.setBounds(10,5,380,360);
            pan.add(border);

            JLabel logo =new JLabel (new ImageIcon(getClass().getResource("course.jpg")));
            JLabel titre =new JLabel ("<html><center>QUIZZ COURSE AUX RÊVES<center></html>") ;
            JLabel info = new JLabel ("<html> Cette année aura lieu la 4ème édition de la Course aux rêves afin de\n" +
                    "récolter des fonds et les reverser à l’association Rêves, et réaliser le souhait d'un enfant malade. Répondez aux 10 questions sur la ville d'Angers et gagnez un T-shirt gratuit pour la course aux Rêves !"
                    + "</html>");
            JLabel contact = new JLabel ("<html>"

                    +"<strong><center>Pages FaceBook</center></strong><br/><br/>"
                    +"Course : <a href=''>https://www.facebook.com/humaniseo</a><br/>"
                    +"Humaniseo  : <a href=''>https://www.facebook.com/LaCourseAuxReves</a>"
                    + "</html>");
            titre.setBounds(100,0,180,120);
            logo.setBounds(110,90,150,90);
            titre.setFont(new Font("Times", Font.BOLD, 16));
            titre.setForeground(new Color(87, 79, 61));
            info.setBounds(40,130,300,200);
            info.setForeground(Color.white);
            info.setFont(new Font("Times", Font.PLAIN, 11));
            border.add(info);
            contact.setBounds(80,270,200,100);
            contact.setForeground(new Color(31, 75, 71));
            contact.setFont(new Font("Times", Font.PLAIN, 9));
            border.add(contact);


            border.add(titre);

            JSeparator sep2 = new JSeparator();

            sep2.setBounds(55,265,250,5);

            border.add(sep2);
            border.add(logo);
            setVisible(true);
        }
    }
