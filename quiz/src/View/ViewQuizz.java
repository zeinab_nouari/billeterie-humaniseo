package View;

import Controller.ActionChoiceHint;
import Controller.ActionChoiceQuizz;
import model.Questions;
import model.Timer;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.* ;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

public class ViewQuizz extends JPanel {
    JLabel question ,
            label,
            hintLabel;

    JButton option1 ,
            option2,
            option3,
            option4 ;

    String correct_answer,
            hint  ;
    JFrame window ;
    JPanel pan = new JPanel () ;
    static boolean next = false ;
    static int score = 0 ;
    static JLabel timer = new JLabel ("00 : 00 : 000") ;
    static model.Timer count = new model.Timer ();
    public ActionChoiceQuizz actionChoiceQuizz = new ActionChoiceQuizz(this);
    public ViewTimer viewTimer ;
    public ViewHint viewHint ;
    public ActionChoiceHint actionChoiceHint ;
    ImageIcon course = new ImageIcon(this.getClass().getResource("courseimage.png"));
    public JButton hintButton = new JButton(new ImageIcon(getClass().getResource("pt.png"))) ;



    public ViewQuizz(Questions obj, JFrame window) {
        this.window = window;
        this.question = new JLabel(obj.question);
        this.hintLabel = new JLabel("");
        this.option1 = new JButton(obj.op1);
        this.option2 = new JButton(obj.op2);
        this.option3 = new JButton(obj.op3);
        this.option4 = new JButton(obj.op4);
        this.correct_answer = obj.correct_answer;
        this.hint = obj.hint;
        this.initComponent();
    }


    public void initComponent() {
        this.window.setVisible(true);
        // fond
        label = new JLabel(course);
        label.setSize(600, 700);
        label.setBounds(0, 0, 600, 700);
        window.setBackground(new Color(39, 138, 191));
        window.setContentPane(this);
        window.add(label);
        this.pan.setLayout(null);
        this.pan.setSize(120, 200);
        this.window.setContentPane(pan);
        setLayout(null);
        setBackground(Color.getHSBColor(154, 254, 25));
        setBounds(0, 0, 600, 700);
        setBorder(BorderFactory.createLineBorder(Color.black));
        this.pan.add(this);

        // Ajout Timer
        this.viewTimer = new ViewTimer(label, count, timer);
        viewTimer.addTimer();

        // Ajout questions et options

        label.add(this.question);
        label.add(option1);
        label.add(option2);
        label.add(option3);
        label.add(option4);

        // Questions & options

        this.question.setBounds(10, 250, 600, 50);
        this.question.setBorder(new LineBorder(new Color(245, 222, 179), 2, true));
        this.question.setHorizontalAlignment(JLabel.CENTER);
        this.question.setForeground(new Color(100,75,60));
        this.question.setFont(new Font("Serif",Font.ITALIC,14));
        option1.setBounds(20, 330, 550, 50);
        this.option1.setForeground(new Color(100,75,60));
        this.option1.setFont(new Font("Serif",Font.ITALIC,14));
        option1.setBackground(new Color(245, 222, 179));
        option2.setBounds(20, 400, 550, 50);
        this.option2.setForeground(new Color(100,75,60));
        this.option2.setFont(new Font("Serif",Font.ITALIC,14));
        option2.setBackground(new Color(245, 222, 179));
        option3.setBounds(20, 470, 550, 50);
        this.option3.setForeground(new Color(100,75,60));
        this.option3.setFont(new Font("Serif",Font.ITALIC,14));
        option3.setBackground(new Color(245, 222, 179));
        option4.setBounds(20, 540, 550, 50);
        this.option4.setForeground(new Color(100,75,60));
        this.option4.setFont(new Font("Serif",Font.ITALIC,14));
        option4.setBackground(new Color(245, 222, 179));

        option1.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));
        option2.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));
        option3.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));
        option4.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));


        // Ajout Hint
        this.viewHint = new ViewHint(label,hintButton,hintLabel);
        viewHint.addHintButton();
        viewHint.addHintLabel();
        this.actionChoiceHint = new ActionChoiceHint(this);

    }

    public void updateAnswer(int time) throws InterruptedException {
        this.actionChoiceHint.showHint(this.hint,hintButton,hintLabel);
        this.actionChoiceQuizz.getAnswer(option1,option2,option3,option4,this.correct_answer,next);
        while (actionChoiceQuizz.getNext() == false ) {
            this.viewTimer.updateTimer(time,timer);
        }
        next = false ;
    }

    public void setHint(){
        hintButton.addActionListener((
                ActionEvent e) -> {
            // hintLabel.setBounds(150,200,200,80);
            hintLabel.setHorizontalAlignment(JLabel.CENTER);
            hintLabel.setForeground(Color.black);
            //hintLabel.setBorder(new LineBorder(Color.black, 2, true));
            hintLabel.setText(hint);
        });

    }


    public  int getScore() {return this.score ;}

    public void setScore(int score){
        this.score = score ;
    }
    public Timer getTime () {return viewTimer.getTime() ;}


}