package View;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import java.awt.*;

public class ViewHint {
    public JLabel hintLabel = new JLabel("Hint");
    public JButton hintBTN = new JButton(new ImageIcon(getClass().getResource("pt.png")));
    public JLabel panel = new JLabel();
    public String hint ;

    public ViewHint(JLabel panel, JButton hintBTN, JLabel hintLabel){
        this.hintBTN = hintBTN ;
        this.hintLabel = hintLabel;
        this.panel = panel ;
        hintBTN.setBounds(270,150,70,70);
        hintBTN.setOpaque(false);
        hintBTN.setContentAreaFilled(false);
        hintBTN.setBorderPainted(false);
        //hintBTN.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));
        hintLabel.setBounds(180, 150, 250, 80);
        hintLabel.setHorizontalAlignment(JLabel.CENTER);
        hintLabel.setForeground(new Color(100,75,60));
        hintLabel.setFont(new Font("Serif",Font.ITALIC,30));
        hintLabel.setBorder(new LineBorder(Color.white, 0, true));

    }

    public void addHintButton(){
        this.panel.add(this.hintBTN);
    }

    public void addHintLabel(){
        this.panel.add(this.hintLabel);
    }

    public JButton getHintBTN(){
        return this.hintBTN ;
    }

    public void setHintLabel(String hint){
        this.hint = hint ;
    }
}
