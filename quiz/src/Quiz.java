import Controller.ActionAddPlayer;
import Controller.ActionChoiceMenu;
import Controller.ActionChoiceQuizz;
import Controller.ActionChoiceScore;
import View.*;
import model.*;
import model.Timer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Quiz {
    public static void main(String[] args) throws InterruptedException {

        String name ;
        String lastname ;
        Connection con = null;
        int score =0 ;


        /*JFrame window = new JFrame("Jeux Quizz");
        window.setSize(400, 300);
        window.setLocationRelativeTo(null);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        Jeux jeux = new Jeux();*/

        ActionAddPlayer action = new ActionAddPlayer(new ViewPlayer());
        String prenom =action.getPrenom();
        String nom =action.getNom();
        Connexionconfig connexionConfig = new Connexionconfig();
        connexionConfig.addPlayerDatabase(action.getPrenom(),action.getNom(),score);

        System.out.println(action.getNom() +" "+ action.getPrenom());
        while (true) {


            int time = 40; //sec
            // La vue et choix du Menu
            JFrame window = new JFrame("Jeux Quizz");
            window.setSize(600, 700);
            window.setLocationRelativeTo(null);
            window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            window.setResizable(false);

            ViewMenu welcome = new ViewMenu(window);
            ActionChoiceMenu actionMenu = new ActionChoiceMenu(welcome);

            actionMenu.choose(time);
            Timer timer = null;

            int nbr=0,m = 0, s = 0;
           // window.setSize(1000, 500);

            Questions[] qObj = {
                    new Questions("Question 1 :   Parmi ces propositions, quelle liste contient uniquement des specialites Angevines?", "1. Le Cointreau, les mogettes, le pâté aux prunes et la soupe Angevine", "2. Le Cointreau, les fouets, la grenade pastille et la soupe Angevine", "3. Le Cointreau, les fouets, le pâté aux prunes et la soupe Angevine ", "4. Il n'y a pas de spécialités culinaires angevines", "1. Le Cointreau, les mogettes, le pâté aux prunes et la soupe Angevine", "Fouace angevine"),
                    new Questions("Question 2 : Le festival des Accroches Coeurs est réputé pour être :", "Un grand festival de théâtre de rues", "Un evenement solidaire et caritatif", "Une opportunite pour les angevins talentueux", "Un évènement pour les malades", "Un grand festival de théâtre de rues"),
                    new Questions("Question 3 : Comment dit-on au revoir à Angers", "Kenavo !", "Topette !", "Balo !", "Aucun de ces trois mots", "Topette !", "C'est le nom d'une fiole"),
                    new Questions("Question 4 : Quelles rivières se rejoignent près de l'île Saint-Aubin pour former la Maine?", "Le Louet et La Sarthe", "L'Oudon et la Mayenne", "La Mayenne et la Sarthe", "op4", "op2", "hint 4"),
                    new Questions("Question 5 : En quelle annee a commence la construction du chateau d’Angers ?", "1301", "1201", "1401", "1901", "1201", "hint 5"),
                    new Questions("Question 6 : Quelle est la capitale de l'Anjou ?", "Oree d'Anjou", "Saumur", "Angers", "Avrille", "Angers", "hint 6"),
                    new Questions("Question 7 : En quel mois se tient les accroches-coeurs ?", "Septembre", "Aout", "Novembre", "Avril", "Septembre", "hint 7"),
                    new Questions("Question 8 : Quel musee n’est pas localise sur Angers ?", "Musee Jean Lurçat", "Musee de l’ardoise", "Musee Pince", "Musee de Bretagne", "Musee de Bretagne", "hint 8"),
                    new Questions("Question 9 : Quelle est la ville la plus proche d’Angers ?", "Avrille", "Beaucouze", "Bouchemaine", "Trelaze", "Avrille", "hint 9"),
                    new Questions("Question 10 : Comment s’appelle le moulin à vent typique de l’Anjou ?", "Le moulin-cavier", "Le moulin angevin", "Le moulin a trois branches", ".  Moulin-Bas de Walheim", "op2", "hint 10")
            };


            while (nbr != qObj.length && (s < time)) {
                ViewQuizz quiz = new ViewQuizz(qObj[nbr], window);
                quiz.updateAnswer(time);
                m = quiz.getTime().m;
                s = quiz.getTime().s;
                score = quiz.getScore();
                System.out.println("s"+score);
                if (nbr == qObj.length - 1 || (s == time)) {
                    quiz.getTime().resetTime();
                    quiz.setScore(0);
                }
                nbr++;

            }

            int nbrQ = qObj.length;
            //scorePane scorePane = new scorePane(window, score, nbrQ);
            // scorePane.choose();
            ViewScore viewS = new ViewScore (window,score) ;
            connexionConfig.addScoreDatabase(nom,prenom,score);
            ActionChoiceScore ActionC = new ActionChoiceScore(viewS);
            ActionC.choose();


        }

    }

}







