CREATE TABLE inscrit_Course (
    personID int,
    nom varchar(25),
    prenom varchar(25),
    mail varchar(25),
    taille Varchar (1) check (taille in ('S','M','L')),
    paiement Varchar (25) check (paiement in ('Carte','Liquide')),
    parcours int check (parcours in ('5','10','15'),
    PRIMARY KEY (PersonID),
    foreign key (parcours),
    
);

CREATE TABLE parcours (
    parcoursID int,
    longueur int check (longueur in ('5','10','15'),
    nombreInscrits int,
    PRIMARY KEY (parcoursID),
);

CREATE TABLE utilisateur (
    id_utilisateur int,
    score int,
    PRIMARY KEY (id_utilisateur),
);
CREATE TABLE listeShirts (
    idShirt int,
    taille int,
    PRIMARY KEY (idShirt),
);
CREATE TABLE joueurs_select (
    joueurID int,
    id_utilisateur int,
    nom varchar(25),
    prenom varchar(25),
    mail varchar(25),
    
    PRIMARY KEY (joueurID),
    foreign key (id_utilisateur),
    
);
