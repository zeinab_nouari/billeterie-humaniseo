#!/bin/bash

## install web service

IP=$(hostname -I | awk '{print $2}')

echo "START - install web Server - "$IP

echo "=> [1]: Installing required packages..."
DEBIAN_FRONTEND=noninteractive
apt-get install -o Dpkg::Progress-Fancy="0" -q -y \
	apache2 \
    	php \
	libapache2-mod-php \
	php-mysql \
	php-intl \
	php-curl \
	php-xmlrpc \
	php-soap \
	php-gd \
	php-json \
	php-cli \
	php-pear \
	php-xsl \
	php-zip \
	php-mbstring \
    	> /tmp/install_web.log 2>&1

echo "=> [2]: Apache2 configuration"

echo "END - install web Server"

