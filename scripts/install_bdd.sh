#!/bin/bash

## install server postgres

IP=$(hostname -I | awk '{print $2}') #permet de récupérer l'IP
USER="projet"
PASSWDDB="network"

echo "START - install MariaDB - "$IP

echo "=> [1]: Install required packages ..."
DEBIAN_FRONTEND=noninteractive
apt-get install -o Dpkg::Progress-Fancy="0" -q >/dev/null 2>&1
apt-get install -o Dpkg::Progress-Fancy="0" -q -y \
	mariadb-server \
	mariadb-client \
       	>> /tmp/install_bdd.log 2>&1

echo "=> [2]: Configuration du service"

echo "=> [3]: Configuration de BDD"

mysql < /vagrant/files/creation_bdd_gpi2.sql >> /tmp/install_bdd.log 2>&1

echo "END - install MariaDB"

