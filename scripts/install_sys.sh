#!/bin/bash

## install base system

IP=$(hostname -I | awk '{print $2}')
NOM="gpi2"
MDP="network"
HDIR="/var/www/html/gpi2"

echo "START - install Server - "$IP

echo "=> [1]: Installing required packages..."
DEBIAN_FRONTEND=noninteractive
apt-get update  -o Dpkg::Progress-Fancy="0" -q -y >> /tmp/install_sys.log 2>&1
# Bug avec GRUB à la mise à jour , a revoir ultérieurement
#apt-get upgrade -o Dpkg::Progress-Fancy="0" -q -y >> /tmp/install_pkg.log 2>&1
apt-get install -o Dpkg::Progress-Fancy="0" -q -y \
	wget \
    	gnupg \
    	>> /tmp/install_sys.log 2>&1

echo "=> [2]: Server configuration"
# Ajout de contrib et non-free pour les depots
sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list
# Ajout de la ligne pour le proxy ESEO mais descativé
echo "#Acquire::http::Proxy \"http://scully.eseo.fr:9999\";" >> /etc/apt/apt.conf
# Pour avoir le clavier en français dans la console VB
sed -i 's/XKBLAYOUT=\"us\"/XKBLAYOUT=\"fr\"/g' /etc/default/keyboard
sed -i 's/XKBVARIANT=\"\"/XKBVARIANT=\"latin9\" /g' /etc/default/keyboard
# ajout utilisateur et autres
mkdir -p $HDIR
adduser --home $HDIR --disabled-password --no-create-home $NOM
echo $NOM:$MDP | chpasswd
chown $NOM $HDIR
chmod 755 $HDIR

echo "END - install Server"

